package TrabalhoChato.trabalhoandroid1705.service

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
import TrabalhoChato.trabalhoandroid1705.entidades.Resultado

interface NewResultsService {

    @Headers("Accept: application/json")
    @GET("top-headlines")

    fun getNews(
        @Query("q")
        busca: String,

        @Query("country")
        pais: String = "br",

        @Query("apiKey")
        apiKey: String = "8aeeaedb98a041509be4fc12354c32fc"

    ): Call<Resultado>

}