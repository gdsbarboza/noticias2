package TrabalhoChato.trabalhoandroid1705.ui

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.carddenoticia.view.*
import TrabalhoChato.trabalhoandroid1705.R
import TrabalhoChato.trabalhoandroid1705.entidades.Noticia
import java.text.SimpleDateFormat


class NoticiaAdapter(private var noticias: MutableList<Noticia>,
                     private var listener: NoticiaListListener
) :

RecyclerView.Adapter<NoticiaAdapter.NoticiaViewHolder>() {
//    var retrofit: Retrofit
//    var service: NewResultsService
//
//    init {
//        retrofit = Retrofit.Builder()
//            .baseUrl(" https://tads2019-todo-list.herokuapp.com/")
//            .addConverterFactory(GsonConverterFactory.create())
//            .build()
//        service = retrofit.create(NewResultsService::class.java)
//    }

//
//    fun carregarNoticia(): Int {
//      noticias = bustaTodas
//    }

    override fun getItemViewType(position: Int) = R.layout.carddenoticia


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        NoticiaViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(viewType, parent, false)
        )

    override fun getItemCount() = noticias.size

    override fun onBindViewHolder(holder: NoticiaViewHolder, position: Int) =
        holder.preencherView(noticias[position])

    inner class NoticiaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun preencherView(noticia: Noticia) {


            itemView.titulo.text = noticia.titulo

            if(noticia.descricao != null) {
                this.itemView.descricao.text = noticia.descricao
            }else {
                itemView.descricao.visibility = View.GONE
            }

            if(noticia.autor != null) {
                itemView.autor.text = noticia.autor
            }else {
                itemView.autor.visibility = View.GONE
            }

            if(noticia.publicado != null) {
                val inFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                val outFormatter = SimpleDateFormat("dd/MM/yyyy HH:mm")

                itemView.publicado.text = outFormatter.format(inFormatter.parse(noticia.publicado))
            }else {
                itemView.publicado.visibility = View.GONE
            }

            if(noticia.urlToImage != null) {
                Picasso.get().load(noticia.urlToImage).into(itemView.imageView2)
                itemView.conteudo.text = noticia.content
            }else {
                itemView.conteudo.visibility = View.GONE
            }

            itemView.btVermais.setOnClickListener {
                val uri = Uri.parse(noticia.url)
                val intent = Intent(Intent.ACTION_VIEW, uri)
                itemView.context.startActivity(intent)
            }

//          itemView.btPesquisa.setOnClickListener {
//              notifyItemChanged()
//              listener.noticiaBusca(noticia)
//          }
            true
        }

    }
}