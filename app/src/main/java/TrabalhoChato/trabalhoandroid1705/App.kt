package TrabalhoChato.trabalhoandroid1705

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        app = this
        appContext = applicationContext
    }

    companion object {
        lateinit var app: App
            private set
        lateinit var appContext: Context
            private set
    }

    val temInternet: Boolean
        get() {
            val cm =
                getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = cm.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }
}
