package TrabalhoChato.trabalhoandroid1705

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import TrabalhoChato.trabalhoandroid1705.entidades.Noticia
import TrabalhoChato.trabalhoandroid1705.entidades.Resultado
import TrabalhoChato.trabalhoandroid1705.service.NewResultsService
import TrabalhoChato.trabalhoandroid1705.ui.NoticiaAdapter
import TrabalhoChato.trabalhoandroid1705.ui.NoticiaListListener

class MainActivity : AppCompatActivity(), NoticiaListListener {
    lateinit var retrofit: Retrofit
    lateinit var service: NewResultsService
    lateinit var adapter: NoticiaAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        configuraRetrofit()
        search.setOnClickListener { carregaDados() }

        carregaDados()
//
//        Picasso
//            .get()
//            .load("")
//            .into(imageView);
    }

    fun configuraRetrofit() {
        retrofit = Retrofit.Builder()
            .baseUrl("https://newsapi.org/v2/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create(NewResultsService::class.java)
    }
//https://newsapi.org/v2/top-headlines?q=apple&apiKey=8aeeaedb98a041509be4fc12354c32fc
    fun carregaDados() {
        service.getNews(busca.text.toString(), this.resources.configuration.locales[0].country).enqueue(object : Callback<Resultado> {
            override fun onFailure(call: Call<Resultado>, t: Throwable) {
                Log.e("ERRRROU", t.message, t)
            }

            override fun onResponse(call: Call<Resultado>, response: Response<Resultado>) {
                val noticias = response.body()?.noticias
                if (noticias != null)
                    configuraRecyclerView(noticias)
            }

        })
    }

    fun configuraRecyclerView(noticias: List<Noticia>) {
        adapter = NoticiaAdapter(noticias.toMutableList(), this)
        noticiaLista.adapter = adapter

        noticiaLista.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)
    }

}
