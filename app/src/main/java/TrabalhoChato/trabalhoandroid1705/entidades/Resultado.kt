package TrabalhoChato.trabalhoandroid1705.entidades

import com.google.gson.annotations.SerializedName

data class Resultado(
    @SerializedName("status")
    var status: String,
    @SerializedName("totalResults")
    var totalResultados: Int,
    @SerializedName("articles")
    var noticias: List<Noticia>
)