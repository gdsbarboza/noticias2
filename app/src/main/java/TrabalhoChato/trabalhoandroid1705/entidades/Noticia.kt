package TrabalhoChato.trabalhoandroid1705.entidades

import com.google.gson.annotations.SerializedName

data class Noticia (
    @SerializedName("author")
    var autor: String?,
    @SerializedName("title")
    var titulo: String,
    @SerializedName("url")
    var url: String,
    @SerializedName("urlToImage")
    var urlToImage: String?,
    @SerializedName("publishedAt")
    var publicado: String?,
    @SerializedName("content")
    var content: String?,
    @SerializedName("description")
    var descricao: String?

) {

    var id: Long = 0

}
